version: '2'
services:
  portainer:
    container_name: portainer
    image: portainer/portainer
    command: -H unix:///var/run/docker.sock
    ports:
      - 9000:9000
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${VOLUME}/portainer/data:/data
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M

  homeassistant:
    container_name: home-assistant
    image: homeassistant/raspberrypi3-homeassistant
    volumes:
      - ${VOLUME}/homeassistant:/config
      - ${VOLUME}/homeassistant/.homeassistant:/home/homeassistant/.homeassistant
      - /etc/localtime:/etc/localtime:ro
    ports:
      - 8123:8123
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M

  node-red:
    container_name: node-red
    image: nodered/node-red
    volumes:
      - ${VOLUME}/node-red/config.json:/data/config.json
    ports:
      - 1880:1880
    environment:
      FLOWS: flows.json
      PGID: 1000
      PUID: 1000
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M

  mosquitto:
    container_name: mosquitto
    image: eclipse-mosquitto
    volumes:
      - ${VOLUME}/mosquitto/config/mosquitto.conf:/mosquitto/config/mosquitto.conf
      - ${VOLUME}/mosquitto/config/:/mosquitto/config/
      - ${VOLUME}/mosquitto/data:/mosquitto/data
      - ${VOLUME}/mosquitto/log:/mosquitto/log
    ports:
      - 1883:1883
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M

  esphome:
    container_name: esphome
    image: esphome/esphome-armhf
    volumes:
      - ${VOLUME}/esphome/config:/config  
    ports:
      - 6052:6052
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M

  pihole:
    container_name: pihole
    image: pihole/pihole
    volumes:
      - ${VOLUME}/pihole:/etc/pihole
      - ${VOLUME}/dnsmasq.d:/etc/dnsmasq.d
    ports:
      - "53:53/tcp"
      - "53:53/udp"
      - "67:67/udp"
      - 80:80
      - 443:443
    environment:
      WEBPASSWORD: ${PASSWORD}
      DNS1: ${DNS_1}
      DNS2: ${DNS_2}
      PGID: 1000
      PUID: 1000
    dns:
      - 127.0.0.1
      - 1.1.1.1
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M

  homebridge:
    image: oznu/homebridge:raspberry-pi
    container_name: homebridge
    environment:
      - TZ=Europe/Paris
      - PGID=1000
      - PUID=1000
      - HOMEBRIDGE_CONFIG_UI=1
      - HOMEBRIDGE_CONFIG_UI_PORT=8080
    volumes:
      - ${VOLUME}/homebridge:/homebridge
    ports:
      - 8080:8080
    restart: unless-stopped
    security_opt:
      - seccomp:unconfined
      - apparmor:docker-default
      - no-new-privileges
    mem_limit: 80M
